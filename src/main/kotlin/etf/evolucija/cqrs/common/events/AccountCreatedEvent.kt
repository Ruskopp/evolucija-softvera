package etf.evolucija.cqrs.common.events

import etf.evolucija.cqrs.framework.common.Event
import java.time.LocalDateTime
import java.util.*

class AccountCreatedEvent(val accountUUID: UUID, val createdAt: LocalDateTime) : Event() {
}