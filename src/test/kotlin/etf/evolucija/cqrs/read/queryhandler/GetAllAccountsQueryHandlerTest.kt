package etf.evolucija.cqrs.read.queryhandler

import etf.evolucija.cqrs.read.dto.AccountDTO
import etf.evolucija.cqrs.read.query.GetAccountQuery
import etf.evolucija.cqrs.read.query.GetAllAccountsQuery
import etf.evolucija.cqrs.read.repository.AccountDTORepository
import org.junit.Assert.*
import org.junit.Test
import java.util.*

class GetAllAccountsQueryHandlerTest {
    @Test
    fun canHandleGetAllAccountsQuery() {
        val handler = GetAllAccountsQueryHandler(AccountDTORepository())
        val query = GetAllAccountsQuery()

        val result = handler.canHandle(query)

        assertTrue(result)
    }

    @Test
    fun cantHandleOtherQuery() {
        val handler = GetAllAccountsQueryHandler(AccountDTORepository())
        val query = GetAccountQuery(UUID.randomUUID())

        val result = handler.canHandle(query)

        assertFalse(result)
    }

    @Test
    fun handle() {
        val acc1 = AccountDTO()
        val acc2 = AccountDTO()
        val acc3 = AccountDTO()
        val repo = AccountDTORepository()
        repo.save(acc1)
        repo.save(acc2)
        repo.save(acc3)
        val handler = GetAllAccountsQueryHandler(repo)
        val query = GetAllAccountsQuery()

        val resultAccounts = handler.handle(query)

        assertEquals(3, resultAccounts.size)
        assertTrue(resultAccounts.contains(acc1))
        assertTrue(resultAccounts.contains(acc2))
        assertTrue(resultAccounts.contains(acc3))
    }

}