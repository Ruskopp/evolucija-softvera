package etf.evolucija.cqrs.framework

import etf.evolucija.cqrs.framework.write.CommandGateway
import etf.evolucija.cqrs.framework.write.CommandHandler
import etf.evolucija.cqrs.write.account.command.DepositCommand
import org.junit.Test
import org.mockito.Mockito.*
import java.util.*

class CommandGatewayTest {

    @Test
    fun testHandleCalledOnEveryHandler() {
        val c1 = mock(CommandHandler::class.java)
        val c2 = mock(CommandHandler::class.java)
        val c3 = mock(CommandHandler::class.java)
        val gate = CommandGateway(listOf(c1, c2, c3))
        val command = DepositCommand(0, UUID.randomUUID())

        gate.send(command)

        verify(c1, times(1)).handle(command)
        verify(c2, times(1)).handle(command)
        verify(c3, times(1)).handle(command)
    }
}