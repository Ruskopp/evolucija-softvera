package etf.evolucija.cqrs.read.queryhandler

import etf.evolucija.cqrs.framework.read.Query
import etf.evolucija.cqrs.framework.read.QueryHandler
import etf.evolucija.cqrs.read.dto.TransactionDTO
import etf.evolucija.cqrs.read.query.GetTransactionQuery
import etf.evolucija.cqrs.read.repository.TransactionDTORepository
import org.springframework.stereotype.Component

@Component
class GetTransactionQueryHandler(val repo: TransactionDTORepository) : QueryHandler() {
    override fun canHandle(q: Query) = q is GetTransactionQuery

    override fun handle(q: Query): TransactionDTO? {
        if (q is GetTransactionQuery) {
            return repo.get(q.transactionId)
        }
        throw RuntimeException("Invalid Query in GetTransactionQueryHandler ...")//TODO make custom exception
    }

}
