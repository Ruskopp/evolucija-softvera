package etf.evolucija.cqrs.write.account.commandhandler

import etf.evolucija.cqrs.write.account.Account
import etf.evolucija.cqrs.write.account.AccountRepository
import etf.evolucija.cqrs.write.account.command.CreateAccountCommand
import etf.evolucija.cqrs.write.account.command.DepositCommand
import org.junit.Assert.*
import org.junit.Test
import java.util.*

class DepositCommandHandlerTest {
    @Test
    fun canHandleDepositCommand() {
        val handler = DepositCommandHandler(AccountRepository())
        val command = DepositCommand(0,UUID.randomUUID())

        val result = handler.canHandle(command)

        assertTrue(result)
    }

    @Test
    fun cantHandleNotDepositCommand() {
        val handler = DepositCommandHandler(AccountRepository())
        val command = CreateAccountCommand()

        val result = handler.canHandle(command)

        assertFalse(result)
    }

    @Test
    fun handleDeposit() {
        val account = Account()
        val repo = AccountRepository()
        repo.save(account)
        val command = DepositCommand(500, account.uuid)
        val handle = DepositCommandHandler(repo)

        handle.handle(command)

        assertEquals(500, account.balance)
    }

}