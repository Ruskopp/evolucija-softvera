package etf.evolucija.cqrs.read.queryhandler

import etf.evolucija.cqrs.framework.read.Query
import etf.evolucija.cqrs.framework.read.QueryHandler
import etf.evolucija.cqrs.read.dto.AccountDTO
import etf.evolucija.cqrs.read.query.GetAllAccountsQuery
import etf.evolucija.cqrs.read.repository.AccountDTORepository
import org.springframework.stereotype.Component

@Component
class GetAllAccountsQueryHandler(val repo: AccountDTORepository) : QueryHandler() {
    override fun canHandle(q: Query) = q is GetAllAccountsQuery

    override fun handle(q: Query): List<AccountDTO> {
        if(q is GetAllAccountsQuery){
            return repo.getAll()
        }
        throw RuntimeException("Invalid query for handler..")//TODO
    }
}