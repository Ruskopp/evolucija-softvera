package etf.evolucija.cqrs.read.repository

import etf.evolucija.cqrs.read.dto.TransactionDTO
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class TransactionDTORepository {
    private val transactions: MutableList<TransactionDTO> = ArrayList()

    fun save(acc: TransactionDTO) {
        transactions.add(acc)
    }

    fun get(uuid: UUID) = transactions.find { tran -> tran.uuid == uuid }

    fun getForAccount(accountUUID: UUID): List<TransactionDTO> = transactions.filter { tran -> tran.accountUUID == accountUUID }

    fun size() = transactions.size

}