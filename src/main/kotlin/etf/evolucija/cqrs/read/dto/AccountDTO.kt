package etf.evolucija.cqrs.read.dto

import java.util.*

class AccountDTO(val uuid: UUID = UUID.randomUUID()) {
    var amount = 0
}