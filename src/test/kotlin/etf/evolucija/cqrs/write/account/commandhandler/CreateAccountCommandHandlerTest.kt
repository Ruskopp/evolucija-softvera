package etf.evolucija.cqrs.write.account.commandhandler

import etf.evolucija.cqrs.write.account.Account
import etf.evolucija.cqrs.write.account.AccountRepository
import etf.evolucija.cqrs.write.account.command.CreateAccountCommand
import etf.evolucija.cqrs.write.account.command.DepositCommand
import org.junit.Assert
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import java.util.*

class CreateAccountCommandHandlerTest {
    @Test
    fun canHandleCreateAccountCommand() {
        val handler = CreateAccountCommandHandler(AccountRepository())
        val command = CreateAccountCommand()

        val result = handler.canHandle(command)

        assertTrue(result)
    }

    @Test
    fun cantHandleOtherCommand() {
        val handler = CreateAccountCommandHandler(AccountRepository())
        val command = DepositCommand(0, UUID.randomUUID())

        val result = handler.canHandle(command)

        assertFalse(result)
    }

    @Test
    fun handleCreateAccount() {
        val repo = AccountRepository()
        val command = CreateAccountCommand()
        val handle = CreateAccountCommandHandler(repo)

        handle.handle(command)

        Assert.assertEquals(1, repo.size())
    }

}