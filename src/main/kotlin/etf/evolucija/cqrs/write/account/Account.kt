package etf.evolucija.cqrs.write.account

import etf.evolucija.cqrs.write.old.WithdrawalException
import java.util.*

open class Account(var balance: Int) {
    val uuid: UUID = UUID.randomUUID()

    constructor() : this(0)

    fun deposit(amount: Int) {
        balance += amount
    }

    open fun withdrawal(amount: Int) {
        if (balance - amount < 0)
            throw WithdrawalException()
        balance -= amount
    }

    fun transfer(accountTo: Account, amount: Int) {
        this.withdrawal(amount)
        accountTo.deposit(amount)
    }
}