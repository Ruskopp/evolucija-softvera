package etf.evolucija.cqrs.api

import etf.evolucija.cqrs.framework.read.QueryGateway
import etf.evolucija.cqrs.read.dto.TransactionDTO
import etf.evolucija.cqrs.read.query.GetAllTransactionsQuery
import etf.evolucija.cqrs.read.query.GetTransactionsForAccountQuery
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/transaction")
class TransactionApi(val queryGateway: QueryGateway) {

    //@GetMapping
    fun getTransactions(): List<TransactionDTO> {
        return queryGateway.send(GetAllTransactionsQuery())
    }

    @GetMapping("/{accountUUID}")
    fun getTransactionsForAccount(@PathVariable accountUUID: UUID): List<TransactionDTO> {
        return queryGateway.send(GetTransactionsForAccountQuery(accountUUID))
    }
}