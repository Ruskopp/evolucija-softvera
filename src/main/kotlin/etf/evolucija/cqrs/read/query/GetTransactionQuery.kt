package etf.evolucija.cqrs.read.query

import etf.evolucija.cqrs.framework.read.Query
import java.util.*

class GetTransactionQuery(val transactionId: UUID) : Query()


