package etf.evolucija

import etf.evolucija.cqrs.write.account.Account
import etf.evolucija.cqrs.write.old.WithdrawalException
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test

class AccountTest {

    @Test
    fun whenCreated_thenUUIDCreated() {
        val account = Account()

        assertNotNull(account.uuid)
    }

    @Test
    fun whenCreatedWithInitialBalance_thenUUIDCreated() {
        val account = Account(500)

        assertNotNull(account.uuid)
    }

    @Test
    fun whenCreated_thenBalanceZero() {
        val account = Account()

        assertEquals(0, account.balance)
    }

    @Test
    fun whenCreatedWith500_thenBalance500() {
        val account = Account(500)

        assertEquals(500, account.balance)
    }

    @Test
    fun whenDeposit500_thenBalance500() {
        val account = Account()

        account.deposit(500)

        assertEquals(500, account.balance)
    }

    @Test
    fun given500_whenWithdrawal200_thenBalance300() {
        val account = Account(500)

        account.withdrawal(200)

        assertEquals(300, account.balance)
    }

    @Test(expected = WithdrawalException::class)
    fun given500_whenWithdrawal600_thenWithdrawalException() {
        val account = Account(500)

        account.withdrawal(600)
    }

    @Test
    fun given500_whenWithdrawal600_thenBalanceNotChanged() {
        val account = Account(500)

        try {
            account.withdrawal(600)
        } catch (e: Exception) {
        }

        assertEquals(500, account.balance)
    }

    @Test
    fun given500_whenTransfer300_thenBalance200() {
        val accountFrom = Account(500)
        val accountTo = Account()

        accountFrom.transfer(accountTo, 300)

        assertEquals(200, accountFrom.balance)
    }

    @Test
    fun given500_whenTransfer300_thenBalanceOnAnother300() {
        val accountFrom = Account(500)
        val accountTo = Account()

        accountFrom.transfer(accountTo, 300)

        assertEquals(300, accountTo.balance)
    }

}
