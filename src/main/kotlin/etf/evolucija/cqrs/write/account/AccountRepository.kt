package etf.evolucija.cqrs.write.account

import org.springframework.stereotype.Repository
import java.util.*

@Repository
class AccountRepository {
    private val accounts: MutableList<Account> = ArrayList()

    fun save(acc: Account) {
        accounts.add(acc)
    }

    fun get(uuid: UUID) = accounts.find { acc -> acc.uuid == uuid }
    fun size() = accounts.size
}