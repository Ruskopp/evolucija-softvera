package etf.evolucija.cqrs.write.account

import org.junit.Assert.*
import org.junit.Test

class AccountRepositoryTest {

    @Test
    fun whenAccountSaved_thenAccountPresent(){
        val repo = AccountRepository()
        val acc = Account()

        repo.save(acc)

        val res = repo.get(acc.uuid)
        assertEquals(acc,res)
    }

    @Test
    fun testSize(){
        val repo = AccountRepository()
        val acc1 = Account()
        val acc2 = Account()
        val acc3 = Account()

        repo.save(acc1)
        repo.save(acc2)
        repo.save(acc3)

        assertEquals(3, repo.size())
    }
}