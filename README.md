# Projekat iz predmeta evolucija softvera
### Ovaj projekat pokriva dve zahtevane teme predmeta:

 1. **TDD (Test driven development)** - metodologija razvoja softvera u kojoj se prvenstveno pisu testovi a tek onda kod koji ih zadovoljava
 
 2. **Refactoring** - refaktorisanje koda predstavlja metodologiju u kojoj se menja napisani kod bez promene njegove funkcionalnosti
 

## Test driven development

TDD se sastoji iz sledecih koraka:

1. Napise se test koji testira funkcionalnost koja jos uvek nije implementirana 
2. Test se pokrene da bi se proverilo da li test pada, ovim korakom se proverava kvalitet napisanog testa, uzimaju u obzir da funkcionalnost jos ne postoji test mora da padne
3. Napise se minmalna kolicina koda za testiranu funkcionalnost
4. Test se pokrene i proveri da li sada test prolazi
5. Nakon par napisanih funkcionalnosti, one se refaktorisu da bi se otimizovao produkcioni kod

TDD pristup programiranju pruza dva velika benefita. Prvi benefit je sto nakon implementiranja trazene funkcionalnosti vec imate testove, jer produkcioni kod se pise samo kao odgovor na napisan test. Imati veliku pokrivenost koda testovima predstavlja prvu karakteristiku kvalitetnog softvera. Drugi benefit TDD-a je sto ovakan nacin razvoja utice na sam dizajn koda, lakse se razdvajaju interfejsi i implementacije.

Alalti korisceni za pisanje testova su JUnit i Mockito.

JUnit je alat koji sluzi za pisanje testova. Mockito je alat koji sluzi za kreiranje Mokova koji su potrebni da bi se testirala neka funkcionalnost

Primeri testova (unit i mini itegracioni):

	@Test
    fun testHandleCalledOnEveryHandler() {
        val c1 = mock(CommandHandler::class.java)
        val c2 = mock(CommandHandler::class.java)
        val c3 = mock(CommandHandler::class.java)
        val gate = CommandGateway(listOf(c1, c2, c3))
        val command = DepositCommand(0, UUID.randomUUID())

        gate.send(command)

        verify(c1, times(1)).handle(command)
        verify(c2, times(1)).handle(command)
        verify(c3, times(1)).handle(command)
    }

	@Test
    fun whenWithdrawalEventAndAccountPresent_thenTransactionCreated() {
        val accountUUID = UUID.randomUUID()
        val accountDTO = AccountDTO(accountUUID)
        accountDTORepository.save(accountDTO)
        val withdrawal = LocalDateTime.now()
        val event = WithdrawalEvent(accountUUID, 500, withdrawal)

        eventBus.send(event)

        val tran = transactionDTORepository.getForAccount(accountUUID).find { tran -> tran.accountUUID == accountUUID && tran.time == withdrawal }
        assertNotNull(tran)
        assertEquals(500, tran?.amount)
        assertEquals(TransactionType.WITHDRAWAL, tran?.type)
    }

## Refactoring

Refaktoring predstavlja metodologiju promene dizajna koda bez promene njegove funkcionalnosti. Neki od razloga sa refaktorisanjem su poboljsanje odrzivosti koda, izbacivanje nepotrebnih delova, uvodjenje projektnih uzoraka koji nisu postojali. Kada se pocinje sa refaktorisanjem od velike je vaznosti da kod bude pokriven testovima, jer ne postoji drugaciji nacin da se osiguramo da nismo promenili neku od funkcionalnosti.

Generalno u programiranju postoji pojam dva sesira tj, jedan za refaktorisanje i jedan za dodavanje novih funkcionalnosti, i programer uvek mora nositi samo jedan. Ovo znaci da kada se dodaju nove funckionalnosti ne menja se postojeci kod, i obrnuto.


#### Uvedeni projekti uzorci CQRS, DAO, Facade, Builder, Decorater

##### Decorater
Uzorak koji dinamicki dodaje funkcionalnost dekorisanom objektu. U ovom projektu dodata funkcionalnost je logovanje.

	class EventHandlerLoggingDecorater(private val handler: EventHandler) : EventHandler() {
	    private val LOGGER = LoggerFactory.getLogger(EventHandlerLoggingDecorater::class.java)
	
	    override fun handle(e: Event) {
	        LOGGER.info("Method handle called on $this.javaClass.simpleName class at ${LocalDateTime.now()}")
	        LOGGER.info("Event passed is ${e.javaClass.simpleName}")
	
	        handler.handle(e)
	
	        LOGGER.info("Execution finished at ${LocalDateTime.now()}")
	    }
	}
##### Builder
Uzorak koji olaksava kreiranje objekta koji ima veliku kolicinu opcionih atributa.
Kod:

	class TransactionDTO private constructor(val uuid: UUID = UUID.randomUUID(),
	                                         val accountUUID: UUID,
	                                         val time: LocalDateTime,
	                                         val amount: Int,
	                                         val type: TransactionType) {
	
	    class Builder() {
	        var uuid: UUID = UUID.randomUUID()
	        var accountUUID: UUID? = null
	        var time: LocalDateTime? = null
	        var amount: Int? = null
	        var type: TransactionType? = null
	
	
	        fun uuid(uuid: UUID): Builder {
	            this.uuid = uuid
	            return this
	        }
	
	        fun accountUUID(accountUUID: UUID): Builder {
	            this.accountUUID = accountUUID
	            return this
	        }
	
	        fun time(time: LocalDateTime): Builder {
	            this.time = time
	            return this
	        }
	
	        fun amount(amount: Int): Builder {
	            this.amount = amount
	            return this
	        }
	
	        fun type(type: TransactionType): Builder {
	            this.type = type
	            return this
	        }
	
	        fun build(): TransactionDTO {
	            val ex = RuntimeException("Builder parameters not set")
	            return TransactionDTO(
	                    uuid,
	                    accountUUID ?: throw ex,
	                    time ?: throw ex,
	                    amount ?: throw ex,
	                    type ?: throw ex
	            )
	        }
	    }
	}
Koriscenje buildera:

	TransactionDTO.Builder()
                    .accountUUID(e.accountUUID)
                    .time(e.depositedAt)
                    .amount(e.amount)
                    .type(TransactionType.DEPOSIT)
                    .build()


##### Facade
Uzorak koji olaksava pristup vecem delu sistema tako sto se postavlja kao jedinstvena tacka pristupa. Kod:

	class ClientFasade {
	    private val evolucijaUrl = "http://localhost:8080";
	    private val restTemplate = RestTemplate()
	
	    init {
	        restTemplate.uriTemplateHandler = DefaultUriBuilderFactory(evolucijaUrl)
	    }
	
	    fun getAccounts(): List<AccountDTO>? {
	        return restTemplate.getForObject("/account")
	    }
	
	    fun getAccount(id: UUID): AccountDTO? {
	        return restTemplate.getForObject("/account/$id")
	    }
	
	    fun createAccount() {
	        restTemplate.postForEntity("/account", "", Any::class.java)
	    }
	
	    fun depositToAccount(uuid: UUID, amount: Int) {
	        restTemplate.postForEntity("/account/deposit/$uuid/$amount", "", Any::class.java)
	    }
	
	    fun withdrawalFromAccount(uuid: UUID, amount: Int) {
	        restTemplate.postForEntity("/account/withdrawal/$uuid/$amount", "", Any::class.java)
	    }
	
	    fun getTransactionsForAccount(accountUUID: UUID): List<TransactionDTO>? {
	        return restTemplate.getForObject("/transaction/$accountUUID")
	    }
	}

##### DAO
Data Access Object predstavlja objekat koji enkapsulira pristup podacima (najcesci slucaj je da su podaci u bazi, ali ne mora da znaci, cesta situacija je da se podaci dohvataju iz nekog ekstenog seriva, ako na primer pricamo o mikroservisima). Kod:

	@Repository
	class TransactionDTORepository {
	    private val transactions: MutableList<TransactionDTO> = ArrayList()
	
	    fun save(acc: TransactionDTO) {
	        transactions.add(acc)
	    }
	
	    fun get(uuid: UUID) = transactions.find { tran -> tran.uuid == uuid }
	
	    fun getForAccount(accountUUID: UUID): List<TransactionDTO> = transactions.filter { tran -> tran.accountUUID == accountUUID }
	
	    fun size() = transactions.size
	
	}

##### CQRS
CQRS arhitektura sistema, ili ti uzorak enterprajz aplikacija je nesto do cega nisam dosao refaktorisanjem vec sam ga implementirao od samog pocetka. CQRS je veoma zanimljiv za evoluciju softvera jer sam njegov nastanak je vezan za posmatranje zivotnog veka velikih sistema i nacina na koji su oni evoluirali kroz vreme.

Osnovni pristup pri dizajniranju velikih sistema je uvek bio CRUD pristup nad jednim modelom, odnosno klasicna troslojna arhitektura (slika ispod). U ovom sistemu postoji jedan model koji podrzava CRUD komande, ondnosno kreiranje, izmenu, brisanje i citanje podataka.
  

![](https://www.techopedia.com/images/uploads/3b108f50042e4c398169ec3fa43d9b94.png)

Kako potrebe sistema rastu pristup podacima se menja. Isti podatak sada moze da se tumaci na vise nacina. Primecuju se dve osnovne operacije nad podacima, write (create update delete) i read. Takodje se primecuju razlike u frekventnosti pristupa podacima, odnosno da se podasci cesce citaju nego upisuju, kao i da ceto citanja sporije traju nego upis.

CQRS resava te probleme jer se kod njega read i write model dele u dva nezavisna modela. Write model je model koji reaguje na komande i nad kojim se vrse operacije upisa, izmene i brisanja. Baza koju koristi write model je normalizovana sql baza koja pruza koinzistenciju podataka. Kada se neka komanda izvrsi u write modelu ona generise dogadjaj na osnovu kog se azurira baza read modela. Read baza je obicno denormalizovana, jako cesto no-sql baza koja pruza brzo citanje podataka. Prilikom reagovanja na dogadjaj podaci se odmah smestaju u read model u onom obliku pogodnom za citanje, da pri citanje ne bi trosili vreme na spajanja tabela i slicno. Read modelu se izdaju takozvani upiti koji samo citaju podatke i vracaju klientu bez ikakve dodatne logike.

Ovaj pristup diazjnu takodje olaksava razvoj jer prilikom implementacije nema potrebe razmisljai odjednom o hiljadu stvari vec je prvo fokus samo na tome kako podatak smestiti u write bazu i minimalnoj kolicini informacija potrebnoj za dalje azuriranje write modela. Nakon toga se procenjuje koje informacije su potrebne za citanje i na osnovu toga konstruisu podaci u read modelu. Poslednji korak je konstrukciaj dogadjaja koji ce generasti komanda na osnovu kod ce se azurirati stanje read modela. 

![](http://i.imgur.com/a5R8oEk.png)

### Ostali alati i tehnologije:
   * SpringBoot framework
   * Kotlin programski jezik
   * Maven alat za konstrukciju projekata i upravljanje zavisnostima

##### SpringBoot
Ovaj framework se sastoji iz raznih modula koji se mogu nezavisno koristiti u aplikaciji. Ovi moduli su jako bitni je predstavlaju implementacije mnogih projektnih uzoraka koji se preko anotacija mogu lako koristiti. Ovaj framework predstavlja dobar primer evolucije softvera gde su se vremenom uocile stvari koje su potrebne i koje nema potrebe implementirati iznova vec samo kristiti. 

###### Depenecy Injection
Jedan od najbitnijih uzoraka u spring boot-u, omogucava jednostavno definisanje komponenti i mesta na kojim se koiriste. Time se olaksava kreiranje raznih delova aplikacije i veci fokus je na poslovnoj logici. Primer:
   
Definisanje servisa koji moze koristiti na drugim mestima jednostavnom anotacijom `@Service`
	
	@Service
	class EventBus @Autowired constructor(private var handlers: List<EventHandler>) {
	    init {
	        handlers = handlers.map { it -> EventHandlerLoggingDecorater(it) }
	    }
	
	    fun send(e: Event) {
	        handlers.forEach { it.handle(e) }
	    }
	}
   

Ovaj se servis onda lako moze koirsiti u drugim delovima anotacijom `@Autowired`. Sve ostalo spring radi sam, pri pokretanju aplikacije on pravi instancu EventBusa i postavlja reference na nju svuda gde je definisan kao `@Autowierd`
	
	abstract class CommandHandler {
	
	    @Autowired
	    private var eventBus: EventBus? = null
	
	    abstract fun canHandle(c: Command): Boolean
	    abstract fun handle(c: Command)
	
	    fun emit(e: Event) {
	        eventBus?.send(e)
	    }
	}

##### Kotlin
Programski jezik koji se kompajlira u bajtkod i izvrsava na Java virtuelnoj masini. Jezik je u potpunosti interoperabilan sa javom, sto znaci da java kod moze pozivati kotlin kod i obrnuto. Inicijalno napravljen da zameni javu na adroid platformi ali u zadnje vreme uzima sve veceg maha u java svetu.

##### Maven
Alat pomocu kojeg se definisu koraci prilikom pravljena .jar fajla i pomocu njega se definisiu zavisnosti koje vasa aplikacija ima prema drugim bibliotekama. Sve zavisnosti i koraci za izgradnju se definisu u pom.xml fajlu. Primer pom fajla:


	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<kotlin.version>1.2.20</kotlin.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.module</groupId>
			<artifactId>jackson-module-kotlin</artifactId>
		</dependency>

 Primer verzije Kotlin jezika na kojoj se projekat kompajlira. Sve ostale zavisnosti ako nemaju definisanu verziju koristi se najnovija.

### Pokretanje

Kao alat za pokretanje je preporucen InteliJ. Pre toga je potrebno imate namesten Maven na racunaru i konfigurisan u okviru InteliJ-a. Nakon sto se projekat otvori pomocu InteliJ-a on ce ga prepoznati kao Maven projekat i na osnovu pom.xml-a preuzeti sve neophodne biblioteke potrebne za rad. 