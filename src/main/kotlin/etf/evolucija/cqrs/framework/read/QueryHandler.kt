package etf.evolucija.cqrs.framework.read

abstract class QueryHandler {
    abstract fun canHandle(q: Query): Boolean
    abstract fun  handle(q: Query): Any?
}