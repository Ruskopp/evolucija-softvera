package etf.evolucija.transactions

import etf.evolucija.cqrs.write.account.Account
import etf.evolucija.cqrs.write.old.DepositTransaction
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.*

class DepositTransactionTest {

    @Test
    fun whenCreated_thenUUIDNotNull() {
        val tran = DepositTransaction(Account(), 0)

        assertNotNull(tran.uuid)
    }

    @Test
    fun whenCreated_thenTimestampNotNull() {
        val tran = DepositTransaction(Account(), 0)

        assertNotNull(tran.timeStamp)
    }

    @Test
    fun givenAccountWith500_whenRollback200_thenBalance300() {
        val accountMock = Mockito.mock(Account::class.java)
        doNothing().`when`(accountMock).withdrawal(200)
        val tran = DepositTransaction(accountMock, 200)

        tran.rollback()

        verify(accountMock, times(1)).withdrawal(200)
    }

}