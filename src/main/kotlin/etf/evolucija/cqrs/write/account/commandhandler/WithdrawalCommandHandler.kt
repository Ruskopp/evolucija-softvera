package etf.evolucija.cqrs.write.account.commandhandler

import etf.evolucija.cqrs.common.events.DepositEvent
import etf.evolucija.cqrs.common.events.WithdrawalEvent
import etf.evolucija.cqrs.framework.write.Command
import etf.evolucija.cqrs.framework.write.CommandHandler
import etf.evolucija.cqrs.write.account.AccountRepository
import etf.evolucija.cqrs.write.account.command.WithdrawalCommand
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class WithdrawalCommandHandler(val repository: AccountRepository) : CommandHandler() {
    override fun canHandle(c: Command) = c is WithdrawalCommand

    override fun handle(c: Command) {
        if (c is WithdrawalCommand) {
            val account = repository.get(c.accountUUID)
            account?.withdrawal(c.ammount)//TODO change this

            if (account != null) {
                emit(WithdrawalEvent(account.uuid, c.ammount, LocalDateTime.now()))
            }
        }
    }
}