package etf.evolucija.cqrs.write.account.commandhandler

import etf.evolucija.cqrs.common.events.DepositEvent
import etf.evolucija.cqrs.framework.write.Command
import etf.evolucija.cqrs.framework.write.CommandHandler
import etf.evolucija.cqrs.write.account.AccountRepository
import etf.evolucija.cqrs.write.account.command.DepositCommand
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class DepositCommandHandler(val repository: AccountRepository) : CommandHandler() {

    override fun canHandle(c: Command) = c is DepositCommand


    override fun handle(c: Command) {
        if (c is DepositCommand) {
            val account = repository.get(c.accountUUID)
            account?.deposit(c.amount)//TODO change this

            if (account != null) {
                emit(DepositEvent(account.uuid, c.amount, LocalDateTime.now()))
            }

        }
    }

}
