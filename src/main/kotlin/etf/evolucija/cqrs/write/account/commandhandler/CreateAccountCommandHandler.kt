package etf.evolucija.cqrs.write.account.commandhandler

import etf.evolucija.cqrs.common.events.AccountCreatedEvent
import etf.evolucija.cqrs.framework.write.Command
import etf.evolucija.cqrs.framework.write.CommandHandler
import etf.evolucija.cqrs.write.account.Account
import etf.evolucija.cqrs.write.account.AccountRepository
import etf.evolucija.cqrs.write.account.command.CreateAccountCommand
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class CreateAccountCommandHandler(val repository: AccountRepository) : CommandHandler() {
    override fun canHandle(c: Command) = c is CreateAccountCommand

    override fun handle(c: Command) {
        if (c is CreateAccountCommand) {
            val acc = Account()
            repository.save(acc)

            emit(AccountCreatedEvent(acc.uuid, LocalDateTime.now()))
        }
    }
}