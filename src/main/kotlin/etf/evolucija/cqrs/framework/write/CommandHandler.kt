package etf.evolucija.cqrs.framework.write

import etf.evolucija.cqrs.framework.common.Event
import etf.evolucija.cqrs.framework.common.EventBus
import org.springframework.beans.factory.annotation.Autowired


abstract class CommandHandler {

    @Autowired
    private var eventBus: EventBus? = null

    abstract fun canHandle(c: Command): Boolean
    abstract fun handle(c: Command)

    fun emit(e: Event) {
        eventBus?.send(e)
    }
}