package etf.evolucija.cqrs.framework.common

import org.slf4j.LoggerFactory
import java.time.LocalDateTime

class EventHandlerLoggingDecorater(private val handler: EventHandler) : EventHandler() {
    private val LOGGER = LoggerFactory.getLogger(EventHandlerLoggingDecorater::class.java)

    override fun handle(e: Event) {
        LOGGER.info("Method handle called on $this.javaClass.simpleName class at ${LocalDateTime.now()}")
        LOGGER.info("Event passed is ${e.javaClass.simpleName}")

        handler.handle(e)

        LOGGER.info("Execution finished at ${LocalDateTime.now()}")
    }
}