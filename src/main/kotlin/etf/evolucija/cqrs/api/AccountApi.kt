package etf.evolucija.cqrs.api

import etf.evolucija.cqrs.framework.read.QueryGateway
import etf.evolucija.cqrs.framework.write.CommandGateway
import etf.evolucija.cqrs.read.dto.AccountDTO
import etf.evolucija.cqrs.read.query.GetAccountQuery
import etf.evolucija.cqrs.read.query.GetAllAccountsQuery
import etf.evolucija.cqrs.write.account.command.CreateAccountCommand
import etf.evolucija.cqrs.write.account.command.DepositCommand
import etf.evolucija.cqrs.write.account.command.WithdrawalCommand
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/account")
class AccountApi(val queryGateway: QueryGateway, val commandGateway: CommandGateway) {

    @GetMapping
    fun getAccounts(): List<AccountDTO> {
        return queryGateway.send(GetAllAccountsQuery())
    }

    @GetMapping("/{id}")
    fun getAccount(@PathVariable id: UUID): AccountDTO {
        return queryGateway.send(GetAccountQuery(id))
    }

    @PostMapping
    fun createAccount() {
        commandGateway.send(CreateAccountCommand())
    }

    @PostMapping("/deposit/{uuid}/{amount}")
    fun depositToAccount(@PathVariable uuid: UUID, @PathVariable amount: Int) {
        commandGateway.send(DepositCommand(amount, uuid))
    }

    @PostMapping("/withdrawal/{uuid}/{amount}")
    fun withdrawalFromAccount(@PathVariable uuid: UUID, @PathVariable amount: Int) {
        commandGateway.send(WithdrawalCommand(amount, uuid))
    }
}