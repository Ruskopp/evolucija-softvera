package etf.evolucija.cqrs.read.queryhandler

import etf.evolucija.cqrs.read.dto.TransactionDTO
import etf.evolucija.cqrs.read.dto.TransactionType
import etf.evolucija.cqrs.read.query.GetAccountQuery
import etf.evolucija.cqrs.read.query.GetTransactionQuery
import etf.evolucija.cqrs.read.repository.TransactionDTORepository
import org.junit.Test

import org.junit.Assert.*
import org.junit.Ignore
import java.time.LocalDateTime
import java.util.*

class GetTransactionQueryHandlerTest {
    @Test
    fun canHandleGetTransactionQuery() {
        val handler = GetTransactionQueryHandler(TransactionDTORepository())
        val query = GetTransactionQuery(UUID.randomUUID())

        val result = handler.canHandle(query)

        assertTrue(result)
    }

    @Test
    fun cantHandleOtherQuery() {
        val handler = GetTransactionQueryHandler(TransactionDTORepository())
        val query = GetAccountQuery(UUID.randomUUID())

        val result = handler.canHandle(query)

        assertFalse(result)
    }

    @Test
    fun handle() {
        val tran = createDefaultTransaction()
        val repo = TransactionDTORepository()
        repo.save(tran)
        val handler = GetTransactionQueryHandler(repo)
        val query = GetTransactionQuery(tran.uuid)

        val tranResult = handler.handle(query)

        assertEquals(tran, tranResult)
    }

    private fun createDefaultTransaction(transactionUUID: UUID = UUID.randomUUID(), accountUUID: UUID = UUID.randomUUID()) =
            TransactionDTO.Builder()
                    .uuid(transactionUUID)
                    .accountUUID(accountUUID)
                    .time(LocalDateTime.now())
                    .amount(500)
                    .type(TransactionType.DEPOSIT)
                    .build()

}