package etf.evolucija

import etf.evolucija.cqrs.api.AccountApi
import etf.evolucija.cqrs.api.TransactionApi
import etf.evolucija.cqrs.read.dto.TransactionType
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class EvolucijaApplicationTests {

    @Autowired
    lateinit var accountApi: AccountApi

    @Autowired
    lateinit var transactionApi: TransactionApi

    @Test
    fun contextLoads() {
    }

    @Test
    fun testMainBehaviour() {
        accountApi.createAccount()
        accountApi.createAccount()
        accountApi.createAccount()

        val createdAccounts = accountApi.getAccounts()

        assertEquals(3, createdAccounts.size)

        val accountUUID = createdAccounts[1].uuid //random ex: 0, 1 or 2

        accountApi.depositToAccount(accountUUID, 500)
        accountApi.depositToAccount(accountUUID, 1300)
        accountApi.depositToAccount(accountUUID, 200)
        accountApi.withdrawalFromAccount(accountUUID, 500)
        accountApi.withdrawalFromAccount(accountUUID, 500)

        val account = accountApi.getAccount(accountUUID)

        assertNotNull(account)
        assertEquals(1000, account.amount)

        val transactions = transactionApi.getTransactionsForAccount(accountUUID)

        assertEquals(5, transactions.size)

        val withdrawalCount = transactions.sumBy { if (it.type == TransactionType.WITHDRAWAL) 1 else 0 }
        val depositCount = transactions.sumBy { if (it.type == TransactionType.DEPOSIT) 1 else 0 }

        assertEquals(2, withdrawalCount)
        assertEquals(3, depositCount)
    }

}
