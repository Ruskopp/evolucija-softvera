package etf.evolucija.cqrs.read.queryhandler

import etf.evolucija.cqrs.read.dto.AccountDTO
import etf.evolucija.cqrs.read.query.GetAccountQuery
import etf.evolucija.cqrs.read.query.GetTransactionQuery
import etf.evolucija.cqrs.read.repository.AccountDTORepository
import org.junit.Test

import org.junit.Assert.*
import java.util.*

class GetAccountQueryHandlerTest {
    @Test
    fun canHandleGetAccountQuery() {
        val handler = GetAccountQueryHandler(AccountDTORepository())
        val query = GetAccountQuery(UUID.randomUUID())

        val result = handler.canHandle(query)

        assertTrue(result)
    }

    @Test
    fun cantHandleOtherQuery() {
        val handler = GetAccountQueryHandler(AccountDTORepository())
        val query = GetTransactionQuery(UUID.randomUUID())

        val result = handler.canHandle(query)

        assertFalse(result)
    }

    @Test
    fun handle() {
        val acc = AccountDTO()
        val repo = AccountDTORepository()
        repo.save(acc)
        val handler = GetAccountQueryHandler(repo)
        val query = GetAccountQuery(acc.uuid)

        val result = handler.handle(query)

        assertEquals(acc, result)
    }

}