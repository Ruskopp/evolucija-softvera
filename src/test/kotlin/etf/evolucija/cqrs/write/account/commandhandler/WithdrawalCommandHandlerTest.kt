package etf.evolucija.cqrs.write.account.commandhandler

import etf.evolucija.cqrs.write.account.Account
import etf.evolucija.cqrs.write.account.AccountRepository
import etf.evolucija.cqrs.write.account.command.DepositCommand
import etf.evolucija.cqrs.write.account.command.WithdrawalCommand
import org.junit.Test

import org.junit.Assert.*
import java.util.*

class WithdrawalCommandHandlerTest {
    @Test
    fun canHandleWithdrawalCommand() {
        val handler = WithdrawalCommandHandler(AccountRepository())
        val command = WithdrawalCommand(0, UUID.randomUUID())

        val result = handler.canHandle(command)

        assertTrue(result)
    }

    @Test
    fun cantHandleNotWithdrawalCommand() {
        val handler = WithdrawalCommandHandler(AccountRepository())
        val command = DepositCommand(0, UUID.randomUUID())

        val result = handler.canHandle(command)

        assertFalse(result)
    }

    @Test
    fun handleWithdrawal() {
        val account = Account(500)
        val repo = AccountRepository()
        repo.save(account)
        val command = WithdrawalCommand(300, account.uuid)
        val handle = WithdrawalCommandHandler(repo)

        handle.handle(command)

        assertEquals(200, account.balance)
    }

}