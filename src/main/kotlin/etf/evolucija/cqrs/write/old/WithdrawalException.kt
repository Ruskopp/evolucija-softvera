package etf.evolucija.cqrs.write.old

class WithdrawalException : RuntimeException("Not enough balance for withdrawal")
