package etf.evolucija

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class EvolucijaApplication

fun main(args: Array<String>) {
    runApplication<EvolucijaApplication>(*args)
}
