package etf.evolucija.cqrs.framework.common

import etf.evolucija.cqrs.common.events.AccountCreatedEvent
import etf.evolucija.cqrs.common.events.DepositEvent
import etf.evolucija.cqrs.common.events.WithdrawalEvent
import etf.evolucija.cqrs.read.dto.AccountDTO
import etf.evolucija.cqrs.read.dto.TransactionType
import etf.evolucija.cqrs.read.repository.AccountDTORepository
import etf.evolucija.cqrs.read.repository.TransactionDTORepository
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import java.time.LocalDateTime
import java.util.*


@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EventBusIntegrationTest {

    @Autowired
    lateinit var eventBus: EventBus

    @Autowired
    lateinit var accountDTORepository: AccountDTORepository
    @Autowired
    lateinit var transactionDTORepository: TransactionDTORepository

    @Test
    fun whenSendAccountCreatedEvent_thenSizeBigger() {
        val initialSize = accountDTORepository.size()

        eventBus.send(AccountCreatedEvent(UUID.randomUUID(), LocalDateTime.now()))
        eventBus.send(AccountCreatedEvent(UUID.randomUUID(), LocalDateTime.now()))
        eventBus.send(AccountCreatedEvent(UUID.randomUUID(), LocalDateTime.now()))

        assertEquals(initialSize + 3, accountDTORepository.size())
    }

    @Test
    fun whenSendAccountCreatedEvent_thenAccountWithSameUUIDPresent() {
        val accountUUID = UUID.randomUUID()
        val createdAt = LocalDateTime.now()
        val ace = AccountCreatedEvent(accountUUID, createdAt)

        eventBus.send(ace)

        val accountDTO = accountDTORepository.get(accountUUID)
        assertNotNull(accountDTO)
    }

    @Test
    fun whenDepositEventAndAccountNotPresent_thenNoExceptions() {

        eventBus.send(DepositEvent(UUID.randomUUID(), 400, LocalDateTime.now()))

        assertTrue(true)
    }

    @Test
    fun whenDepositEventAndAccountPresent_thenAmountBigger() {
        val accountUUID = UUID.randomUUID()
        val accountDTO = AccountDTO(accountUUID)
        accountDTORepository.save(accountDTO)
        val createdAt = LocalDateTime.now()
        val event = DepositEvent(accountUUID, 500, createdAt)

        eventBus.send(event)

        assertEquals(500, accountDTO.amount)
    }

    @Test
    fun whenDepositEventAndAccountPresent_thenTransactionCreated() {
        val accountUUID = UUID.randomUUID()
        val accountDTO = AccountDTO(accountUUID)
        accountDTORepository.save(accountDTO)
        val depositedAt = LocalDateTime.now()
        val event = DepositEvent(accountUUID, 500, depositedAt)

        eventBus.send(event)

        val tran = transactionDTORepository.getForAccount(accountUUID).find { tran -> tran.accountUUID == accountUUID && tran.time == depositedAt }
        assertNotNull(tran)
        assertEquals(500, tran?.amount)
        assertEquals(TransactionType.DEPOSIT, tran?.type)
    }

    @Test
    fun whenWithdrawalEventAndAccountPresent_thenAmountSmaller() {
        val accountUUID = UUID.randomUUID()
        val accountDTO = AccountDTO(accountUUID)
        accountDTO.amount = 500
        accountDTORepository.save(accountDTO)
        val withdrawalAt = LocalDateTime.now()
        val event = WithdrawalEvent(accountUUID, 300, withdrawalAt)

        eventBus.send(event)

        assertEquals(200, accountDTO.amount)
    }

    @Test
    fun whenWithdrawalEventAndAccountPresent_thenTransactionCreated() {
        val accountUUID = UUID.randomUUID()
        val accountDTO = AccountDTO(accountUUID)
        accountDTORepository.save(accountDTO)
        val withdrawal = LocalDateTime.now()
        val event = WithdrawalEvent(accountUUID, 500, withdrawal)

        eventBus.send(event)

        val tran = transactionDTORepository.getForAccount(accountUUID).find { tran -> tran.accountUUID == accountUUID && tran.time == withdrawal }
        assertNotNull(tran)
        assertEquals(500, tran?.amount)
        assertEquals(TransactionType.WITHDRAWAL, tran?.type)
    }

}