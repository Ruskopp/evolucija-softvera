package etf.evolucija.client

import etf.evolucija.cqrs.read.dto.AccountDTO
import etf.evolucija.cqrs.read.dto.TransactionDTO
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForObject
import org.springframework.web.util.DefaultUriBuilderFactory
import java.util.*

class ClientFasade {
    private val evolucijaUrl = "http://localhost:8080";
    private val restTemplate = RestTemplate()

    init {
        restTemplate.uriTemplateHandler = DefaultUriBuilderFactory(evolucijaUrl)
    }

    fun getAccounts(): List<AccountDTO>? {
        return restTemplate.getForObject("/account")
    }

    fun getAccount(id: UUID): AccountDTO? {
        return restTemplate.getForObject("/account/$id")
    }

    fun createAccount() {
        restTemplate.postForEntity("/account", "", Any::class.java)
    }

    fun depositToAccount(uuid: UUID, amount: Int) {
        restTemplate.postForEntity("/account/deposit/$uuid/$amount", "", Any::class.java)
    }

    fun withdrawalFromAccount(uuid: UUID, amount: Int) {
        restTemplate.postForEntity("/account/withdrawal/$uuid/$amount", "", Any::class.java)
    }

    fun getTransactionsForAccount(accountUUID: UUID): List<TransactionDTO>? {
        return restTemplate.getForObject("/transaction/$accountUUID")
    }
}