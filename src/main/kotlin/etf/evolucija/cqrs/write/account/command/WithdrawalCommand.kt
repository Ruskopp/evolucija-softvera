package etf.evolucija.cqrs.write.account.command

import etf.evolucija.cqrs.framework.write.Command
import java.util.*

class WithdrawalCommand(val ammount: Int, val accountUUID: UUID) : Command {
}