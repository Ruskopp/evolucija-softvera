package etf.evolucija.cqrs.framework.write

import org.springframework.stereotype.Service

@Service
class CommandGateway constructor(val commandHandlers: List<CommandHandler>) {
    fun send(c: Command) {
        commandHandlers.forEach {
            it.handle(c)
        }
    }
}