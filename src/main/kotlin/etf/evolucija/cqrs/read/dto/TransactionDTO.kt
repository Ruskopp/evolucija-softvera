package etf.evolucija.cqrs.read.dto

import java.time.LocalDateTime
import java.util.*

enum class TransactionType {
    WITHDRAWAL, DEPOSIT
}

class TransactionDTO private constructor(val uuid: UUID = UUID.randomUUID(),
                                         val accountUUID: UUID,
                                         val time: LocalDateTime,
                                         val amount: Int,
                                         val type: TransactionType) {

    class Builder() {
        var uuid: UUID = UUID.randomUUID()
        var accountUUID: UUID? = null
        var time: LocalDateTime? = null
        var amount: Int? = null
        var type: TransactionType? = null


        fun uuid(uuid: UUID): Builder {
            this.uuid = uuid
            return this
        }

        fun accountUUID(accountUUID: UUID): Builder {
            this.accountUUID = accountUUID
            return this
        }

        fun time(time: LocalDateTime): Builder {
            this.time = time
            return this
        }

        fun amount(amount: Int): Builder {
            this.amount = amount
            return this
        }

        fun type(type: TransactionType): Builder {
            this.type = type
            return this
        }

        fun build(): TransactionDTO {
            val ex = RuntimeException("Builder parameters not set")
            return TransactionDTO(
                    uuid,
                    accountUUID ?: throw ex,
                    time ?: throw ex,
                    amount ?: throw ex,
                    type ?: throw ex
            )
        }
    }

}