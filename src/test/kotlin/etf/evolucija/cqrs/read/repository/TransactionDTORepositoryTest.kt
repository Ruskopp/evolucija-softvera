package etf.evolucija.cqrs.read.repository

import etf.evolucija.cqrs.read.dto.TransactionDTO
import etf.evolucija.cqrs.read.dto.TransactionType
import org.junit.Assert.*
import org.junit.Test
import java.time.LocalDateTime
import java.util.*

class TransactionDTORepositoryTest {
    @Test
    fun whenAccountSaved_thenAccountPresent() {
        val repo = TransactionDTORepository()
        val tran = createDefaultTransaction()

        repo.save(tran)

        val res = repo.get(tran.uuid)
        assertEquals(tran, res)
    }

    @Test
    fun testSize() {
        val repo = TransactionDTORepository()
        val tran1 = createDefaultTransaction()
        val tran2 = createDefaultTransaction()
        val tran3 = createDefaultTransaction()

        repo.save(tran1)
        repo.save(tran2)
        repo.save(tran3)

        assertEquals(3, repo.size())
    }

    @Test
    fun testGetTransactionsForAccount() {
        val accountUUID = UUID.randomUUID()
        val tran1 = createDefaultTransaction(accountUUID = accountUUID)
        val tran2 = createDefaultTransaction(accountUUID = accountUUID)
        val tran3 = createDefaultTransaction(accountUUID = accountUUID)
        val tranDif1 = createDefaultTransaction()
        val tranDif2 = createDefaultTransaction()
        val repo = TransactionDTORepository()
        repo.save(tran1)
        repo.save(tranDif1)
        repo.save(tran2)
        repo.save(tranDif2)
        repo.save(tran3)


        val result = repo.getForAccount(accountUUID)

        assertEquals(3, result.size)
        assertTrue(result.contains(tran1))
        assertTrue(result.contains(tran2))
        assertTrue(result.contains(tran3))

    }

    private fun createDefaultTransaction(transactionUUID: UUID = UUID.randomUUID(), accountUUID: UUID = UUID.randomUUID()) =
            TransactionDTO.Builder()
                    .uuid(transactionUUID)
                    .accountUUID(accountUUID)
                    .time(LocalDateTime.now())
                    .amount(500)
                    .type(TransactionType.DEPOSIT)
                    .build()
}