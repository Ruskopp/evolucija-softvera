package etf.evolucija.cqrs.common

import etf.evolucija.cqrs.common.events.AccountCreatedEvent
import etf.evolucija.cqrs.common.events.DepositEvent
import etf.evolucija.cqrs.common.events.WithdrawalEvent
import etf.evolucija.cqrs.framework.common.Event
import etf.evolucija.cqrs.framework.common.EventHandler
import etf.evolucija.cqrs.read.dto.AccountDTO
import etf.evolucija.cqrs.read.dto.TransactionDTO
import etf.evolucija.cqrs.read.dto.TransactionType
import etf.evolucija.cqrs.read.repository.AccountDTORepository
import etf.evolucija.cqrs.read.repository.TransactionDTORepository
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

@Component
class EventHandlerImpl(val accountRepo: AccountDTORepository, val transactionRepo: TransactionDTORepository) {

    @Bean
    fun accountCreatedHandler() = object : EventHandler() {
        override fun handle(e: Event) {
            if (e is AccountCreatedEvent) {
                accountRepo.save(AccountDTO(e.accountUUID))
            }
        }
    }

    @Bean
    fun depositHandler() = object : EventHandler() {
        override fun handle(e: Event) {
            if (e is DepositEvent) {
                val acc = accountRepo.get(e.accountUUID)
                val amount = acc?.amount
                acc?.amount = (amount ?: 0) + e.amount

                transactionRepo
                        .save(
                                TransactionDTO.Builder()
                                        .accountUUID(e.accountUUID)
                                        .time(e.depositedAt)
                                        .amount(e.amount)
                                        .type(TransactionType.DEPOSIT)
                                        .build()
                        )
            }
        }
    }

    @Bean
    fun withdrawalHandler() = object : EventHandler() {
        override fun handle(e: Event) {
            if (e is WithdrawalEvent) {
                val acc = accountRepo.get(e.accountUUID)
                val amount = acc?.amount
                acc?.amount = (amount ?: 0) - e.amount

                transactionRepo
                        .save(
                                TransactionDTO.Builder()
                                        .accountUUID(e.accountUUID)
                                        .time(e.withdrawaldAt)
                                        .amount(e.amount)
                                        .type(TransactionType.WITHDRAWAL)
                                        .build()
                        )
            }
        }
    }
}