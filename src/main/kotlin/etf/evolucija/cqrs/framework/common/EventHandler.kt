package etf.evolucija.cqrs.framework.common

abstract class EventHandler {
    abstract fun handle(e: Event)
}