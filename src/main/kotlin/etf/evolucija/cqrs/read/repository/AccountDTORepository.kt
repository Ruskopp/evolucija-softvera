package etf.evolucija.cqrs.read.repository

import etf.evolucija.cqrs.read.dto.AccountDTO
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class AccountDTORepository {
    private val accounts: MutableList<AccountDTO> = ArrayList()

    fun save(acc: AccountDTO) {
        accounts.add(acc)
    }

    fun get(uuid: UUID) = accounts.find { acc -> acc.uuid == uuid }
    fun size() = accounts.size
    fun getAll(): List<AccountDTO> {
        return accounts
    }
}