package etf.evolucija.cqrs.read.queryhandler

import etf.evolucija.cqrs.framework.read.Query
import etf.evolucija.cqrs.framework.read.QueryHandler
import etf.evolucija.cqrs.read.dto.AccountDTO
import etf.evolucija.cqrs.read.query.GetAccountQuery
import etf.evolucija.cqrs.read.repository.AccountDTORepository
import org.springframework.stereotype.Component

@Component
class GetAccountQueryHandler(val repo: AccountDTORepository) : QueryHandler() {
    override fun canHandle(q: Query): Boolean {
        return q is GetAccountQuery
    }

    override fun handle(q: Query): AccountDTO? {
        if(q is GetAccountQuery){
            return repo.get(q.accountId)
        }
        throw RuntimeException("Invalid query for handler")//TODO create custom exception
    }
}