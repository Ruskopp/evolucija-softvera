package etf.evolucija.cqrs.read.repository

import etf.evolucija.cqrs.read.dto.AccountDTO
import org.junit.Assert.*
import org.junit.Test

class AccountDTORepositoryTest {
    @Test
    fun whenAccountSaved_thenAccountPresent() {
        val repo = AccountDTORepository()
        val acc = AccountDTO()

        repo.save(acc)

        val res = repo.get(acc.uuid)
        assertEquals(acc, res)
    }

    @Test
    fun testSize() {
        val repo = AccountDTORepository()
        val acc1 = AccountDTO()
        val acc2 = AccountDTO()
        val acc3 = AccountDTO()

        repo.save(acc1)
        repo.save(acc2)
        repo.save(acc3)

        assertEquals(3, repo.size())
    }
}