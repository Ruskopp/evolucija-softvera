package etf.evolucija.cqrs.write.account.command

import etf.evolucija.cqrs.framework.write.Command
import java.util.*

class DepositCommand(val amount: Int, val accountUUID: UUID) : Command {
    val uuid = UUID.randomUUID()
}