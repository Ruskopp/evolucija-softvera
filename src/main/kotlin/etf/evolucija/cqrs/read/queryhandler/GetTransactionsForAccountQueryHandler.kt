package etf.evolucija.cqrs.read.queryhandler

import etf.evolucija.cqrs.framework.read.Query
import etf.evolucija.cqrs.framework.read.QueryHandler
import etf.evolucija.cqrs.read.dto.TransactionDTO
import etf.evolucija.cqrs.read.query.GetTransactionsForAccountQuery
import etf.evolucija.cqrs.read.repository.TransactionDTORepository
import org.springframework.stereotype.Component

@Component
class GetTransactionsForAccountQueryHandler(val repo: TransactionDTORepository) : QueryHandler() {
    override fun canHandle(q: Query) = q is GetTransactionsForAccountQuery


    override fun handle(q: Query): List<TransactionDTO> {
        if (q is GetTransactionsForAccountQuery) {
            return repo.getForAccount(q.acaountUUID)
        }
        throw RuntimeException("bad query for query handler")
    }
}