package etf.evolucija.cqrs.framework

import etf.evolucija.cqrs.framework.write.CommandGateway
import org.hamcrest.Matchers.greaterThan
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CommandGatewayIntegrationTest {

    @Autowired
    lateinit var gateway: CommandGateway

    @Test
    fun commandsExist() {
        assertThat(gateway.commandHandlers.size ,greaterThan(0))
    }
}