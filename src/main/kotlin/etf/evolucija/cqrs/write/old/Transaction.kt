package etf.evolucija.cqrs.write.old

import etf.evolucija.cqrs.write.account.Account
import java.time.LocalDateTime
import java.util.*

abstract class Transaction(val account: Account, val ammount: Int) {
    val uuid: UUID = UUID.randomUUID()
    val timeStamp: LocalDateTime = LocalDateTime.now()

    abstract fun rollback()
}