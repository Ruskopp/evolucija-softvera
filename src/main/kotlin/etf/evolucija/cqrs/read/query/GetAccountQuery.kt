package etf.evolucija.cqrs.read.query

import etf.evolucija.cqrs.framework.read.Query
import java.util.*

class GetAccountQuery(val accountId : UUID) : Query()