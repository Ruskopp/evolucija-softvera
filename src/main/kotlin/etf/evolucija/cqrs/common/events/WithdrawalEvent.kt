package etf.evolucija.cqrs.common.events

import etf.evolucija.cqrs.framework.common.Event
import java.time.LocalDateTime
import java.util.*

class WithdrawalEvent(val accountUUID: UUID, val amount: Int, val withdrawaldAt: LocalDateTime): Event() {
}