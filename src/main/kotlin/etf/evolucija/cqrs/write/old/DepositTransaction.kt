package etf.evolucija.cqrs.write.old

import etf.evolucija.cqrs.write.account.Account

class DepositTransaction(account: Account, amount: Int) : Transaction(account, amount) {
    override fun rollback() {
       account.withdrawal(ammount)
    }
}