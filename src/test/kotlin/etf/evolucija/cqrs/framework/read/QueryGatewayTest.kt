package etf.evolucija.cqrs.framework.read

import etf.evolucija.cqrs.read.dto.AccountDTO
import etf.evolucija.cqrs.read.dto.TransactionDTO
import etf.evolucija.cqrs.read.query.GetAccountQuery
import etf.evolucija.cqrs.read.queryhandler.GetAccountQueryHandler
import etf.evolucija.cqrs.read.queryhandler.GetAllAccountsQueryHandler
import etf.evolucija.cqrs.read.query.GetTransactionQuery
import etf.evolucija.cqrs.read.queryhandler.GetTransactionQueryHandler
import etf.evolucija.cqrs.read.repository.AccountDTORepository
import etf.evolucija.cqrs.read.repository.TransactionDTORepository
import org.junit.Assert.assertNotNull
import org.junit.Ignore
import org.junit.Test
import java.util.*

class QueryGatewayTest {

    @Test
    @Ignore("Not relevant currently")
    fun testGetAccountQueryMatchHandler() {
        val gate = QueryGateway(listOf(GetAccountQueryHandler(AccountDTORepository()), GetAllAccountsQueryHandler(AccountDTORepository())))

        val a: AccountDTO = gate.send(GetAccountQuery(UUID.randomUUID()))

        assertNotNull(a)
    }

    @Test
    @Ignore("Not relevant currently")
    fun testGetTransactionQueryMatchHandler() {
        val gate = QueryGateway(listOf(GetAccountQueryHandler(AccountDTORepository()), GetTransactionQueryHandler(TransactionDTORepository())))

        val t: TransactionDTO = gate.send(GetTransactionQuery(UUID.randomUUID()))

        assertNotNull(t)
    }


}

