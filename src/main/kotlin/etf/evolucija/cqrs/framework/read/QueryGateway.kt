package etf.evolucija.cqrs.framework.read

import org.springframework.stereotype.Service

@Service
class QueryGateway(val handlers: List<QueryHandler>) {

    final inline fun <reified T : Any, R : Query> send(query: R): T {

        var result: T? = null
        handlers.forEach({
            if (it.canHandle(query)) {
                result = T::class.javaObjectType.cast(it.handle(query))
            }
        })

        return result ?: throw RuntimeException()
    }

}