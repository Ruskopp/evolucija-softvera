package etf.evolucija.cqrs.framework.common

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class EventBus @Autowired constructor(private var handlers: List<EventHandler>) {
    init {
        handlers = handlers.map { it -> EventHandlerLoggingDecorater(it) }
    }

    fun send(e: Event) {
        handlers.forEach { it.handle(e) }
    }
}